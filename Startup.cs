using System;
using Hangfire;
using Hangfire.PostgreSql;
using Hangfire.Storage;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace hangfiretest
{
    public class Startup
    {
        public const string ConnectionString = "Host=localhost;Database=Hangfire2;Username=neovachome;Password=neovac";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            
            services.AddHangfire(config =>
            {
                config.UsePostgreSqlStorage(ConnectionString, new PostgreSqlStorageOptions
                {
                    //QueuePollInterval = TimeSpan.FromSeconds(10),
                    //InvisibilityTimeout = TimeSpan.FromMinutes(1),
                    //DistributedLockTimeout = TimeSpan.FromHours(24),
                    //SchemaName = "hangfire",
                    //PrepareSchemaIfNecessary = true,
                    //UseNativeDatabaseTransactions = true,
                });
            });

            services.AddTransient<IStartupFilter, HangfireStartupFilter>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseHangfireServer(new BackgroundJobServerOptions
            {
                Queues = new[] { "default", "indexes", "emails" },
                HeartbeatInterval = TimeSpan.FromMinutes(1),
                SchedulePollingInterval = TimeSpan.FromSeconds(10),
                WorkerCount = 5
            });

            app.UseHangfireDashboard("/hangfire", new DashboardOptions());
        }
    }

    public class HangfireStartupFilter : IStartupFilter
    {
        private readonly IRecurringJobManager recurringJobManager;
        private readonly JobStorage jobStorage;
        public HangfireStartupFilter(IRecurringJobManager recurringJobManager, JobStorage jobStorage)
        {
            this.recurringJobManager = recurringJobManager;
            this.jobStorage = jobStorage;
        }
        public Action<IApplicationBuilder> Configure(Action<IApplicationBuilder> next)
        {
            return builder =>
            {
                next(builder);
                //after a rename or removal of a recurring job it still remains in the Hangfire db and Hangfire tries to execute it
                //-> remove all recurring jobs whose type can't be loaded
                using (var connection = jobStorage.GetConnection())
                {
                    foreach (var job in connection.GetRecurringJobs())
                    {
                        if (job.LoadException != null)
                            recurringJobManager.RemoveIfExists(job.Id);
                    }
                }
                recurringJobManager.AddOrUpdate<FinkZeitSync>(nameof(FinkZeitSync), p => p.Process(JobCancellationToken.Null), "0 0 * * *");
            };
        }
    }

    public class FinkZeitSync
    {
        public void Process(IJobCancellationToken @null)
        {

        }
    }
}
